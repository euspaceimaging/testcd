const express = require('express');
const port = 7777;
const app = express();

app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.render('index.html');
});

app.listen(port, () => {
    console.log("APP IS RUNNING ON PORT " + port);
});